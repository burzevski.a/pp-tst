// ////////////////////////////////////////////////
//
// EDIT CONFIG OBJECT BELOW !!!
//
// jsConcatFiles => list of javascript files (in order) to concatenate
// buildFilesFoldersRemove => list of files to remove when running final build
// // //////////////////////////////////////////////

var config = {
	jsConcatFiles: [
		'app/js/blazy.min.js',
		'app/js/edit/main.js'

	],
	buildFilesFoldersRemove:[
		'build/scss/',
		'build/js/!(*.min.js)',
		'build/bower.json',
		'build/bower_components/',
		'build/maps/'
	]
};


// ////////////////////////////////////////////////
// Required taskes
// gulp build
// bulp build:serve
// // /////////////////////////////////////////////

var gulp = require('gulp'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	autoprefixer = require('gulp-autoprefixer'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	nunjucksRender = require('gulp-nunjucks-render'),
	htmlmin = require('gulp-htmlmin'),
	imagemin = require('gulp-imagemin'),
	del = require('del');


// ////////////////////////////////////////////////
// Log Errors
// // /////////////////////////////////////////////

function errorlog(err){
	console.error(err.message);
	this.emit('end');
}


// ////////////////////////////////////////////////
// Scripts Tasks
// ///////////////////////////////////////////////

gulp.task('scripts', function() {
  return gulp.src(config.jsConcatFiles)
	.pipe(sourcemaps.init())
		.pipe(concat('temp.js'))
		.pipe(uglify())
		.on('error', errorlog)
		.pipe(rename('app.min.js'))
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('dist/js/'))

    .pipe(reload({stream:true}));
});


// ////////////////////////////////////////////////
// Styles Tasks
// ///////////////////////////////////////////////

gulp.task('styles', function() {
	gulp.src('app/css/scss/style.scss')
		.pipe(sourcemaps.init())
			.pipe(sass({outputStyle: 'compressed'}))
			.on('error', errorlog)
			.pipe(autoprefixer({
	            browsers: ['last 3 versions'],
	            cascade: false
	        }))
		.pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest('dist/css'))
		.pipe(reload({stream:true}));
});


gulp.task('atf', function() {
	gulp.src('app/css/scss/atf.scss')
		.pipe(sourcemaps.init())
			.pipe(sass({outputStyle: 'compressed'}))
			.on('error', errorlog)
			.pipe(autoprefixer({
	            browsers: ['last 3 versions'],
	            cascade: false
	        }))
		.pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest('app/_templates/atf'))
		.pipe(reload({stream:true}));
});

// ////////////////////////////////////////////////
// HTML Tasks
// // /////////////////////////////////////////////

gulp.task('html', function(){
    gulp.src('**/*.html')
    .pipe(reload({stream:true}));
});

// minify
gulp.task('minify', function() {
  return gulp.src('app/*.html')
    .pipe(htmlmin({
			collapseWhitespace:true,
			removeComments:true,
			html5:true,
			caseSensitive:true,
			removeAttributeQuotes:true
		}))
    .pipe(gulp.dest('dist'));
});

// ////////////////////////////////////////////////
// nunjucks
// // /////////////////////////////////////////////

gulp.task('nunjucks', function() {
  // Gets .html and .nunjucks files in pages
  return gulp.src('app/_pages/**/*.+(html|nunjucks|njk|css)')
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['app/_templates']
    }))
  // output files in app folder
  .pipe(gulp.dest('app'))
	.pipe(reload({stream:true}));
});
// ////////////////////////////////////////////////
// Images Tasks
// // /////////////////////////////////////////////

gulp.task('imagemin', function() {
  return gulp.src('app/img/*')
		.pipe(imagemin([
			imagemin.gifsicle({interlaced: true}),
			imagemin.jpegtran({progressive: true}),
			imagemin.optipng({optimizationLevel: 5}),
			imagemin.svgo({
					plugins: [
							{removeViewBox: true},
							{cleanupIDs: false}
					]
			})
			]))
      .pipe(gulp.dest('dist/img'))
});
// ////////////////////////////////////////////////
// Browser-Sync Tasks
// // /////////////////////////////////////////////

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./dist/"
        }
    });
});





// ////////////////////////////////////////////////
// Watch Tasks
// // /////////////////////////////////////////////

gulp.task ('watch', function(){
	gulp.watch('css/**/*.scss', ['styles']);
	gulp.watch('css/**/*.scss', ['atf']);
	gulp.watch('js/edit/*.js', ['scripts']);
  gulp.watch('**/*.html', ['html']);
	gulp.watch('app/**/*.njk', ['nunjucks']);
	gulp.watch('app/**/*.html', ['minify']);
	gulp.watch('app/images/*', ['imagemin']);
});


gulp.task('default', ['scripts', 'styles', 'atf', 'html', 'browser-sync','nunjucks','minify','imagemin', 'watch']);
